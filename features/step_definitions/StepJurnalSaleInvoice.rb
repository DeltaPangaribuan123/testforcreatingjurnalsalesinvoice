require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver.for:chrome 
wait = Selenium::WebDriver::Wait.new(:timeout => 5) 

Given('User launch login page') do
    driver.navigate.to "http://sandbox.jurnal.id/users/login"
end

Given('User enter email') do
    input = wait.until {
        element = driver.find_element(:name, "user[email]")
        element if element.displayed?
        }
        input.send_keys("deltaflorentina123@gmail.com")
end

Given('User enter password') do
    input = wait.until {
        element = driver.find_element(:name, "user[password]")
        element if element.displayed?
        }
        input.send_keys("delta123")
end

When('User clicked Sign in button') do
    driver.find_element(:name, "commit").click
end

Then('User click Sales menu') do
    driver.find_element(:link, "Sales").click
end

Then('User click +Create New Sale button') do
    driver.find_element(:xpath, '//*[@id="main-content"]')
end

Then('User select dropdown Sales Invoice') do
    driver.navigate.to "https://sandbox.jurnal.id/invoices/new"
end

Then('User select dropdown existing customer on the list') do
    driver.find_element(:id, "select2-chosen-19").click
    
end

Then('User input date') do
    input = wait.until {
        element = driver.find_element(:name, "transaction[transaction_date]")
        element if element.displayed?
        }
        input.send_keys("04/11/2020")
end

Then('User select date by date modal')do
    table = wait.until {
        element = driver.find_element(:xpath, '//*[@id="main_form"]/div[3]/div[2]/div/div[1]/div/span')
        element if element.displayed?
}
end
Then('User select dropdown existing product on the list')do
    driver.find_element(:xpath, '//*[@id="select2-drop-mask"]')
end

Then('User input unit price')do
    input = wait.until {
    element = driver.find_element(:name, "transaction[transaction_lines_attributes][0][rate]")
    element if element.displayed?
    }
    input.send_keys("500000")
end
