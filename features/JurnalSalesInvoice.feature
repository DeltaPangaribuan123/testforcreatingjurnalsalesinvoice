Feature: Feature Jurnal Sale Invoice
 
  Scenario: Feature Jurnal Sale Invoice Scenario
    Given User launch login page
    And User enter email
    And User enter password
    When User clicked Sign in button
    Then User click Sales menu
    And User click +Create New Sale button
    And User select dropdown Sales Invoice 
    And User select dropdown existing customer on the list
    And User input date
    And User select date by date modal
    And User select dropdown existing product on the list
    And User input unit price
